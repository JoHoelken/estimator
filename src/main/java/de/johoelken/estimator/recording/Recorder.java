package de.johoelken.estimator.recording;

import de.johoelken.estimator.db.Transaction;
import de.johoelken.estimator.models.Task;

public class Recorder {

    private Task activeTask;

    private Task runningTask;

    private long startTime;

    int unitFactor = 1000;

    private final Transaction transaction;

    public Recorder(Transaction transaction) {
        this.transaction = transaction;
        startTime = 0L;
    }

    public void setActiveTask(Task activeTask) {
        this.activeTask = activeTask;
    }

    public Task getActiveTask() {
        return activeTask;
    }

    public Task getRunningTask() {
        return runningTask;
    }

    public void startRecording() {
        if(activeTask == null) {
            return;
        }
        stopRecording();

        this.runningTask = activeTask;
        this.startTime = System.currentTimeMillis();
    }

    public void stopRecording() {
        if(runningTask == null) {
            return;
        }

        runningTask.setActualTime(runningTask.getActualTime() + getRecordedTime());
        transaction.start();
        transaction.update(runningTask);
        transaction.end();

        runningTask = null;
        startTime = 0L;
    }

    public long getRecordedTime() {
        return (System.currentTimeMillis() - startTime) / unitFactor;
    }

    public boolean isRunningTask(Task other) {
        if(runningTask == null || other == null) {
            return false;
        }
        return runningTask.getId() == other.getId();
    }

}
