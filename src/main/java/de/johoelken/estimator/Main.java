package de.johoelken.estimator;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Loader loader = new Loader();
        Parent root;
        try {
            root = loader.loadRoot();
        } catch (RoutingException e) {
            root = loader.stackTraceView(e);
        }

        Scene scene = new Scene(root, 400, 450);
        scene.getStylesheets().add("estimator.css");

        primaryStage.setTitle("Estimator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
