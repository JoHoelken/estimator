package de.johoelken.estimator.controller;

import de.johoelken.estimator.db.DefaultTransaction;
import de.johoelken.estimator.db.Transaction;

/**
 * Base controller for JavaFX application.
 *
 * @author JoHoelken
 */
public abstract class BaseController {

    private ApplicationController application;
    private Transaction transaction;

    public void setApplication(ApplicationController application) {
        this.application = application;
    }

    protected ApplicationController application() {
        return application;
    }

    protected Transaction getTransaction() {
        if(transaction == null) {
            transaction = new DefaultTransaction();
        }
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
