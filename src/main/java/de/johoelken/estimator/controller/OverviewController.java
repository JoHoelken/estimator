package de.johoelken.estimator.controller;

import de.johoelken.estimator.db.Transaction;
import de.johoelken.estimator.models.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

/**
 * Controller for JavaFX application.
 * Responsible for new task overview.
 *
 * @author JoHoelken
 */
public class OverviewController extends BaseController {

    @FXML
    private TableView taskTable;

    @FXML
    public void initialize() {
        loadTasks();
    }

    public void loadTasks() {
        Transaction transaction = getTransaction();
        transaction.start();
        List tasks = transaction.findByQuery("SELECT t FROM Task t WHERE t.active = true");
        transaction.end();

        if(tasks == null || tasks.isEmpty()) {
            return;
        }

        if(!(tasks.get(0) instanceof Task)){
            application().setErrorMessage("Error: Could not retrieve Tasks.");
            return;
        }

        resetTable();
        ObservableList<Task> tableRows = FXCollections.observableArrayList();
        for(Object entry : tasks){
            tableRows.add((Task) entry);
        }
        taskTable.setItems(tableRows);
    }

    public void updateSelectedTask(Event event) {
        application().setActiveTask((Task) taskTable.getSelectionModel().getSelectedItem());
    }

    private void resetTable() {
        if(taskTable.getColumns().size() > 0) {
            return;
        }

        String[] header = { "Name", "Est.", "S1", "S2", "Actual", "" };
        String[] properties = { "taskName", "estimationFormat", "sigmaOneFormat", "sigmaTwoFormat", "actualTimeFormat", "recording" };

        ObservableList<TableColumn> columns = FXCollections.observableArrayList();
        for(int i=0; i<header.length; i++) {
            TableColumn col = new TableColumn(header[i]);
            col.setCellValueFactory(new PropertyValueFactory<>(properties[i]));
            columns.add(col);
        }

        taskTable.getColumns().addAll(columns);
    }

    public void setTaskTable(TableView taskTable) {
        this.taskTable = taskTable;
    }
}
