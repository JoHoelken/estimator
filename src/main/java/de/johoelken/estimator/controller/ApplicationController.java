package de.johoelken.estimator.controller;

import de.johoelken.estimator.Loader;
import de.johoelken.estimator.PathTo;
import de.johoelken.estimator.RoutingException;
import de.johoelken.estimator.db.DefaultTransaction;
import de.johoelken.estimator.models.Task;
import de.johoelken.estimator.recording.Recorder;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Controller for the Application frame.
 * Holds the methods to replace the main content and error messages.
 *
 * @author JoHoelken
 */
public class ApplicationController {

    @FXML
    BorderPane body;

    @FXML
    Button overviewBtn;

    @FXML
    Label activeTaskName;

    @FXML
    Button activeTaskRecButton;

    @FXML
    Button activeTaskStopButton;

    private Loader loader;

    Recorder recorder;

    @FXML
    public void initialize() {
        setActiveTask(null);
        openOverview(null);
    }

    /**
     * Loads the view into the main panel.
     * @param target the view to load.
     */
    public void updateCenter(PathTo target) {
        resetErrorMessage();
        overviewBtn.setDisable(false);
        try {
            FXMLLoader fxmlLoader = getLoader().getFXMLLoader(target);
            if(!(fxmlLoader.getController() instanceof BaseController)) {
                throw new RoutingException("Wired controller is not an Estimator Controller.");
            }
            else {
                ((BaseController) fxmlLoader.getController()).setApplication(this);
            }
            body.setCenter(fxmlLoader.getRoot());
        } catch (RoutingException e) {
            setErrorMessage(e.getMessage());
        }
    }

    /**
     * Loads the form to create a new task.
     * @param event (unused)
     */
    @FXML
    protected void createNewTask(ActionEvent event){
        updateCenter(PathTo.NEW_TASK);
    }

    /**
     * (Re-)loads the overview page.
     * @param event
     */
    @FXML
    protected void openOverview(ActionEvent event){
        updateCenter(PathTo.OVERVIEW);
        overviewBtn.setDisable(true);
    }

    private void resetErrorMessage(){
        body.setBottom(new VBox());
    }

    /**
     * Displays the given text in a RED box at the bottom of the page.
     * @param message the message to print.
     */
    public void setErrorMessage(String message){
        VBox canvas = new VBox();
        canvas.setStyle("-fx-background-color: #F03434;");

        Text text = new Text(message);
        canvas.getChildren().addAll(text);
        body.setBottom(canvas);
    }

    public void setLoader(Loader loader) {
        this.loader = loader;
    }

    private Loader getLoader() {
        if(loader == null) {
            loader = new Loader();
        }
        return loader;
    }

    void setActiveTask(Task task) {
        getRecorder().setActiveTask(task);
        if(task == null){
            activeTaskName.setText("");
            activeTaskName.setVisible(false);
            activeTaskRecButton.setVisible(false);
            activeTaskStopButton.setVisible(false);
        }
        else {
            activeTaskName.setText(task.getTaskName());
            activeTaskName.setVisible(true);
            activeTaskRecButton.setVisible(!getRecorder().isRunningTask(task));
            activeTaskStopButton.setVisible(getRecorder().isRunningTask(task));
        }
    }

    public void startRecording(Event event) {
        getRecorder().startRecording();
        activeTaskRecButton.setVisible(false);
        activeTaskStopButton.setVisible(true);
    }

    public void stopRecording(Event event) {
        getRecorder().stopRecording();
        activeTaskRecButton.setVisible(true);
        activeTaskStopButton.setVisible(false);
    }

    Task getActiveTask() {
        return getRecorder().getActiveTask();
    }

    private Recorder getRecorder() {
        if(recorder == null) {
            recorder = new Recorder(new DefaultTransaction());
        }
        return recorder;
    }
}
