package de.johoelken.estimator.controller;

import de.johoelken.estimator.db.Transaction;
import de.johoelken.estimator.models.Task;
import de.johoelken.estimator.utils.Formatter;
import de.johoelken.estimator.utils.Validator;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Controller for JavaFX application.
 * Responsible for new task form.
 *
 * @author JoHoelken
 */
public class NewTaskController extends BaseController {

    @FXML
    private TextField taskName;

    @FXML
    private TextArea taskDescription;

    @FXML
    private TextField bestCase;

    @FXML
    private TextField nominalCase;

    @FXML
    private TextField worstCase;

    @FXML
    private TextField estimation;

    @FXML
    private TextField stdDeviation;

    private double estimate;
    private double deviation;

    @FXML
    protected void createTask(ActionEvent event) {
        try {
            validateInput();
            computeEstimation(event);
            Transaction transaction = getTransaction();
            transaction.start();
            transaction.store(taskFromInput());
            transaction.end();
            application().openOverview(event);
        } catch (IllegalArgumentException e) {
            application().setErrorMessage(e.getMessage());
        }
    }

    private Task taskFromInput() {
        Task task = new Task();
        task.setStdDeviation(deviation);
        task.setEstimation(estimate);
        task.setTaskName(taskName.getText());
        task.setTaskDescription(taskDescription.getText());
        return task;
    }

    void validateInput() {
        validateMetadata();
        validateEstination();
    }

    private void validateMetadata() {
        if (Validator.isEmptyOrNull(taskName.getText())) {
            throw new IllegalArgumentException("Input Error: Task Name must not be empty.");
        }
    }

    private void validateEstination() {
        if (Validator.isEmptyOrNull(bestCase.getText())) {
            throw new IllegalArgumentException("Input Error: Best Case must not be empty.");
        }
        int best = parse(bestCase);

        if (Validator.isEmptyOrNull(nominalCase.getText())) {
            throw new IllegalArgumentException("Input Error: Nominal Case must not be empty.");
        }
        int nominal = parse(nominalCase);
        if (best >= nominal) {
            throw new IllegalArgumentException("Input Error: Nominal Case must be greater than Best Case.");
        }
        if (Validator.isEmptyOrNull(worstCase.getText())) {
            throw new IllegalArgumentException("Input Error: Worst Case must not be empty.");
        }
        int worst = parse(worstCase);
        if (nominal >= worst) {
            throw new IllegalArgumentException("Input Error: Worst Case must be greater than Nominal Case.");
        }
    }

    private int parse(TextField field) {
        try {
            return Integer.parseInt(field.getText());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(String.format("Value '%s' in field '%s' is not a number", field.getText(), field.getId()), e);
        }
    }

    @FXML
    protected void computeEstimation(Event event) {
        try {
            validateEstination();
            int best = parse(bestCase);
            int nominal = parse(nominalCase);
            int worst = parse(worstCase);
            estimate = (best + (4 * nominal) + worst) * 1.0 / 6.0;
            deviation = (worst - best) * 1.0 / 6.0;
            estimation.setText(Formatter.toWorkingDayFormat((long) (estimate * Formatter.WORKING_DAY_SECONDS), false));
            stdDeviation.setText(Formatter.toWorkingDayFormat((long) (deviation * Formatter.WORKING_DAY_SECONDS), false));
        } catch (IllegalArgumentException e) {
            estimation.setText("N/A");
            stdDeviation.setText("N/A");
        }
    }

    public void setTaskName(TextField taskName) {
        this.taskName = taskName;
    }

    public void setTaskDescription(TextArea taskDescription) {
        this.taskDescription = taskDescription;
    }

    public void setBestCase(TextField bestCase) {
        this.bestCase = bestCase;
    }

    public void setNominalCase(TextField nominalCase) {
        this.nominalCase = nominalCase;
    }

    public void setWorstCase(TextField worstCase) {
        this.worstCase = worstCase;
    }

    public void setEstimation(TextField estimation) {
        this.estimation = estimation;
    }

    public void setStdDeviation(TextField stdDeviation) {
        this.stdDeviation = stdDeviation;
    }
}
