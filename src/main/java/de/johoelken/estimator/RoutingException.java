package de.johoelken.estimator;

public class RoutingException extends Exception {

    public RoutingException(String message) {
        super(message);
    }

    RoutingException(Throwable throwable) {
        super(throwable);
    }
}
