package de.johoelken.estimator.db;

import de.johoelken.estimator.models.ApplicationModel;

import java.util.List;

/**
 * CRUD Transaction provider for JPA Models
 * @author JoHoelken
 */
public interface Transaction {

    String UNIT_NAME = "default";

    /**
     * Creates a Transaction on the Database via
     * JavaPersistenceAPI (JPA)
     */
    void start();

    /**
     * Commits all changes made.
     */
    void commit();


    /**
     * Commits all changes made and terminates the
     * Connection.
     */
    void end();

    /**
     * Stores the given Model Instance in the Database
     * @param model the Object to store.
     */
    void store(ApplicationModel model);

    /**
     * Updates the given Model Instance in the database
     * @param model the Object to update.
     */
    void update(ApplicationModel model);

    /**
     * Deletes the given Model Instance from the database
     * @param model the Object to delete
     */
    void delete(ApplicationModel model);

    /**
     * Find all Models matching the given String.
     * @param jpql the JPQL String (SQL dialect for JPA)
     * @return the entries matching the query.
     */
    List findByQuery(String jpql);
    <T extends ApplicationModel> T findById(Class<T> clazz, long id);

}
