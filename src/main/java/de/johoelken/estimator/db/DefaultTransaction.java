package de.johoelken.estimator.db;

import de.johoelken.estimator.models.ApplicationModel;

import javax.persistence.*;
import java.util.List;

/**
 * CRUD Transaction provider for JPA Models
 * @author JoHoelken
 */
public class DefaultTransaction implements Transaction {

    private EntityManagerFactory factory;
    private EntityManager entityManager;
    private EntityTransaction transaction;

    private String unitName;

    public DefaultTransaction() {
        this(UNIT_NAME);
    }

    DefaultTransaction(String unitName){
        this.unitName = unitName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        factory = Persistence.createEntityManagerFactory(unitName);
        entityManager = factory.createEntityManager();
        transaction = entityManager.getTransaction();
        transaction.begin();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends ApplicationModel> T findById(Class<T> clazz, long id) {
        return entityManager.find(clazz, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List findByQuery(String jpql) {
        Query q = entityManager.createQuery(jpql);
        return q.getResultList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void store(ApplicationModel model) {
        entityManager.persist(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(ApplicationModel model) {
        entityManager.merge(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(ApplicationModel model) {
        entityManager.remove(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void end() {
        transaction.commit();
        entityManager.close();
        factory.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commit() {
        transaction.commit();
        transaction.begin();
    }


}
