package de.johoelken.estimator;

/**
 * Routing for JavaFX application.
 *
 * @author JoHoelken
 */
public enum PathTo {
    ROOT("main"),
    NEW_TASK("Task/new"),
    OVERVIEW("Home/index");

    private static final String VIEWS_FOLDER = "views";

    private String path;

    PathTo(String path){
        this.path = path;
    }

    public String resourcePath() {
        return VIEWS_FOLDER + "/" + path + ".fxml";
    }

}
