package de.johoelken.estimator;

import de.johoelken.estimator.controller.ApplicationController;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

/**
 * Wrapper around the JavaFX resource loader.
 *
 * @author JoHoelken
 */
public class Loader {

    /**
     * Loads the root view of the application
     * @return the root node.
     * @throws RoutingException iff the root view cannot be loaded.
     */
    Parent loadRoot() throws RoutingException {
        FXMLLoader loader = getFXMLLoader(PathTo.ROOT);
        if(!(loader.getController() instanceof ApplicationController)) {
            throw new RoutingException("Root view is not wired to ApplicationController");
        }
        ((ApplicationController) loader.getController()).setLoader(this);
        return loader.getRoot();
    }

    /**
     * Creates a {@link FXMLLoader} instance for the requested Target.
     * @param target the requested target
     * @return the loader instance
     * @throws RoutingException iff the view cannot be loaded.
     */
    public FXMLLoader getFXMLLoader(final PathTo target) throws RoutingException {
        Objects.requireNonNull(target, "Cannot root to a null Tar");
        try {
            URL view = loadView(target.resourcePath());
            FXMLLoader loader = new FXMLLoader(view);
            loader.load();
            return loader;
        } catch (IllegalArgumentException | IOException e) {
            throw new RoutingException(e);
        }

    }

    private URL loadView(String viewPath) {
        URL view = Loader.class.getClassLoader().getResource(viewPath);
        if (view == null) {
            throw new IllegalArgumentException("Loader error: View '" + viewPath + "' does not exist!");
        }
        return view;
    }

    /**
     * Generates a basic node element to display a stack trace
     * @param t the throwable to print
     * @return the node object.
     */
    public Parent stackTraceView(Throwable t) {
        t.printStackTrace(); //TODO user logger instead.

        final Text body = new Text();
        StringBuilder text = new StringBuilder();
        text.append("### FATAL ERROR ####");
        text.append("\n\n");
        text.append(t.getMessage());
        text.append("\n\n");
        for(StackTraceElement e : t.getStackTrace()) {
            text.append(String.format("%s#%s(%s)%n", e.getClassName(), e.getMethodName(), e.getLineNumber()));
        }
        body.setText(text.toString());
        return createCanvas(body);
    }

    private Parent createCanvas(Node shape){
        VBox canvas = new VBox();
        canvas.setSpacing(10);
        canvas.setPadding(new Insets(8,8,8,8));
        canvas.getChildren().addAll(shape);
        return canvas;
    }

}
