package de.johoelken.estimator.utils;

/**
 * Formatting Utility
 *
 * @author JoHoelken
 */
public final class Formatter {

    private static final int MINUTE_SECONDS = 60;
    private static final int HOUR_SECONDS = 3600;
    public static final int WORKING_DAY_SECONDS = 8 * HOUR_SECONDS;

    private Formatter() {
        // Hide constructor
    }


    public static String toWorkingDayFormat(long seconds, boolean showMinutes) {
        long rest = seconds;
        long days = rest / WORKING_DAY_SECONDS;

        rest = (rest - days * WORKING_DAY_SECONDS);
        long hours = rest / HOUR_SECONDS;

        if(!showMinutes) {
            return String.format("%sd %sh", days, hours);
        }

        rest = (rest - hours * HOUR_SECONDS);
        long minutes = rest / MINUTE_SECONDS;

        return String.format("%sd %sh %sm", days, hours, minutes);
    }
}
