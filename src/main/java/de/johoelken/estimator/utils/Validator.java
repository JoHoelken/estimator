package de.johoelken.estimator.utils;

/**
 * Validation Utility
 *
 * @author JoHoelken
 */
public final class Validator {

    private Validator () {
    }

    /**
     * Checks if the given String is blank (= null, empty or only whitespace)
     * @param string the {@link String} to check
     * @return true iff the string is blank
     */
    public static boolean isEmptyOrNull(String string) {
        return string == null || string.trim().isEmpty();
    }

}
