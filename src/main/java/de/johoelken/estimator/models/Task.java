package de.johoelken.estimator.models;

import de.johoelken.estimator.utils.Formatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * JPA model for JavaFX application.
 *
 * @author JoHoelken
 */
@Entity
public class Task extends ApplicationModel {

    public Task() {
        active = true;
        actualTime = 0L;
    }

    @Id
    @Column(nullable=false)
    @GeneratedValue
    private long id;

    @Column(nullable=false)
    private String taskName;

    @Column
    private String taskDescription;

    @Column(nullable=false)
    private double estimation;

    @Column(nullable=false)
    private double stdDeviation;

    @Column
    private long actualTime;

    @Column
    private boolean active;

    public long getId() {
        return id;
    }

    public void setEstimation(double estimation) {
        this.estimation = estimation;
    }

    public double getEstimation() {
        return estimation;
    }

    public String getEstimationFormat() {
        return Formatter.toWorkingDayFormat((long) (estimation * Formatter.WORKING_DAY_SECONDS), false);
    }

    public String getActualTimeFormat() {
        return Formatter.toWorkingDayFormat(actualTime, true);
    }

    public double getSigmaOne() {
        return estimation + stdDeviation;
    }

    public String getSigmaOneFormat() {
        return Formatter.toWorkingDayFormat((long) (getSigmaOne() * Formatter.WORKING_DAY_SECONDS), false);
    }

    public double getSigmaTwo() {
        return estimation + 2 * stdDeviation;
    }

    public String getSigmaTwoFormat() {
        return Formatter.toWorkingDayFormat((long) (getSigmaTwo() * Formatter.WORKING_DAY_SECONDS), false);
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public double getStdDeviation() {
        return stdDeviation;
    }

    public void setStdDeviation(double stdDeviation) {
        this.stdDeviation = stdDeviation;
    }

    public long getActualTime() {
        return actualTime;
    }

    public void setActualTime(long actualTime) {
        this.actualTime = actualTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
