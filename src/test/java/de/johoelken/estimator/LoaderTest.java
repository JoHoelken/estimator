package de.johoelken.estimator;

import de.johoelken.estimator.helper.FXInitializer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.text.Text;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoaderTest {

    private Loader loader;

    @BeforeClass
    public static void setup() throws InterruptedException {
        FXInitializer.run();
    }

    @BeforeMethod
    public void fixture() {
        loader = new Loader();
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void itDoesNotAllowNullTargets() throws RoutingException {
        loader.getFXMLLoader(null);
    }

    @Test
    public void itDoesLoadExistingViewsWithoutExceptions() throws RoutingException {
        for(PathTo target : PathTo.class.getEnumConstants()){
            FXMLLoader fxmlLoader = loader.getFXMLLoader(target);
            Assert.assertNotNull(fxmlLoader);
        }
    }

    @Test
    public void itDoesLoadRootView() throws RoutingException {
            Assert.assertNotNull(loader.loadRoot());
    }

    @Test
    public void itPrintsTheStackTrace() {
        Parent object = loader.stackTraceView(new RuntimeException("Expected Unit Test Exception"));
        boolean found = false;
        for(Node node :object.getChildrenUnmodifiable()){
            if(node instanceof Text) {
                found = true;
                String text = ((Text) node).getText();
                Assert.assertTrue(text.contains("Expected Unit Test Exception"));
                Assert.assertTrue(text.contains(this.getClass().getName()));
            }
        }
        Assert.assertTrue(found, "Did not find text object in result.");
    }

}