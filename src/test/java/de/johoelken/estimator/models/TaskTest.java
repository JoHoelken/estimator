package de.johoelken.estimator.models;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TaskTest {

    private Task task;

    @BeforeMethod
    public void setup() {
        task = new Task();
    }

    @Test
    public void itCanInitialize() {
        Assert.assertTrue(task instanceof  ApplicationModel);
    }

    @Test
    public void itCanSetTaskName() {
        task.setTaskName("Test Name");
        Assert.assertEquals("Test Name", task.getTaskName());
    }

    @Test
    public void itCanSetTaskDescription() {
        task.setTaskDescription("Test Description");
        Assert.assertEquals("Test Description", task.getTaskDescription());
    }

    @Test
    public void itCanSetEstimation() {
        task.setEstimation(3.45);
        Assert.assertEquals(3.45, task.getEstimation());
    }

    @Test
    public void itCanSetStandardDeviation() {
        task.setStdDeviation(3.45);
        Assert.assertEquals(3.45, task.getStdDeviation());
    }

    @Test
    public void itCanSetActualTime() {
        task.setActualTime(12345L);
        Assert.assertEquals(12345L, task.getActualTime());
    }

    @Test
    public void itDoesNotHaveAnIdIfNotStored() {
        Assert.assertEquals(0L, task.getId());
    }

    @Test
    public void itCanSetToInactive() {
        task.setActive(false);
        Assert.assertFalse(task.isActive());
    }

    @Test
    public void itIsInitializedActive() {
        Assert.assertTrue(task.isActive());
    }

    @Test
    public void itComputesAWorkingDayTimeFormatFromSeconds() {
        long oneDayFourHoursTenMinutes = 28800 + 14400 + 600;
        task.setActualTime(oneDayFourHoursTenMinutes);
        Assert.assertEquals(task.getActualTimeFormat(), "1d 4h 10m");
    }

    @Test
    public void itComputesAWorkingDayTimeFormatFromSecondsWithNoEntry() {
        Assert.assertEquals(task.getActualTimeFormat(), "0d 0h 0m");
    }

    @Test
    public void itComputesSigmaOne() {
        double estimation = 1.23;
        double deviation = 4.56;
        task.setStdDeviation(deviation);
        task.setEstimation(estimation);
        Assert.assertEquals(task.getSigmaOne(), estimation + deviation);
    }

    @Test
    public void itComputesSigmaTwo() {
        double estimation = 1.23;
        double deviation = 4.56;
        task.setStdDeviation(deviation);
        task.setEstimation(estimation);
        Assert.assertEquals(task.getSigmaTwo(), estimation + 2 * deviation);
    }

    @Test
    public void itFormatsEstimation() {
        double estimation = 1.5;
        task.setEstimation(estimation);
        Assert.assertEquals(task.getEstimationFormat(), "1d 4h");
    }

    @Test
    public void itFormatsSigmaOne() {
        double estimation = 1.5;
        double deviation = 1.0;
        task.setEstimation(estimation);
        task.setStdDeviation(deviation);
        Assert.assertEquals(task.getSigmaOneFormat(), "2d 4h");
    }

    @Test
    public void itFormatsSigmaTwo() {
        double estimation = 1.5;
        double deviation = 1.0;
        task.setEstimation(estimation);
        task.setStdDeviation(deviation);
        Assert.assertEquals(task.getSigmaTwoFormat(), "3d 4h");
    }


}