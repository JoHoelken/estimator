package de.johoelken.estimator;

import org.testng.annotations.Test;

public class RoutingExceptionTest {

    @Test(expectedExceptions = RoutingException.class, expectedExceptionsMessageRegExp = "Unit Test Exception")
    public void itCanBeThrownFromAStringMessage() throws RoutingException {
        throw new RoutingException("Unit Test Exception");
    }

    @Test(expectedExceptions = RoutingException.class, expectedExceptionsMessageRegExp = "java.lang.Exception: Unit Test Exception")
    public void itCanBeThrownFromAnotherThrowable() throws RoutingException {
        throw new RoutingException(new Exception("Unit Test Exception"));
    }

}