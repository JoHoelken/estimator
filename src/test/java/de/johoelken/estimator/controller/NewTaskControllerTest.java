package de.johoelken.estimator.controller;

import de.johoelken.estimator.db.Transaction;
import de.johoelken.estimator.helper.FXInitializer;
import de.johoelken.estimator.models.ApplicationModel;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class NewTaskControllerTest {

    @Mock
    private TextField taskName;
    @Mock
    private TextArea taskDescription;
    @Mock
    private TextField bestCase;
    @Mock
    private TextField nominalCase;
    @Mock
    private TextField worstCase;
    @Mock
    private TextField estimation;
    @Mock
    private TextField stdDeviation;
    @Mock
    private ApplicationController app;
    @Mock
    private Transaction transaction;

    private NewTaskController controller;

    private static final String ANY_STRING = "Unit Test Test String";

    @BeforeClass
    public static void setup() throws InterruptedException {
        FXInitializer.run();
    }

    @BeforeMethod
    public void fixture() {
        MockitoAnnotations.initMocks(this);

        controller = new NewTaskController();
        controller.setApplication(app);
        controller.setTransaction(transaction);

        controller.setBestCase(bestCase);
        controller.setNominalCase(nominalCase);
        controller.setWorstCase(worstCase);
        controller.setEstimation(estimation);
        controller.setStdDeviation(stdDeviation);
        controller.setTaskName(taskName);
        controller.setTaskDescription(taskDescription);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testEmptyInputIsNotValid() {
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testTaskNameAloneIsNotValid() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNameAndBestIsNotValid() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("2");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNameBestAndNominalIsNotValid() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("2");
        controller.validateInput();
    }

    @Test
    public void testNameBestWorstAndNominalIsValid() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("2");
        when(worstCase.getText()).thenReturn("3");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testBestAndNominalMustBeInAscending1() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("1");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testBestAndNominalMustBeInAscending2() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("2");
        when(nominalCase.getText()).thenReturn("1");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNominalAndWorstMustBeInAscending1() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("2");
        when(worstCase.getText()).thenReturn("2");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNominalAndWorstMustBeInAscending2() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("3");
        when(worstCase.getText()).thenReturn("2");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testBestMustBeANumber() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("foo");
        when(nominalCase.getText()).thenReturn("1");
        when(worstCase.getText()).thenReturn("2");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testNominalMustBeANumber() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("bar");
        when(worstCase.getText()).thenReturn("2");
        controller.validateInput();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testWorstMustBeANumber() {
        when(taskName.getText()).thenReturn(ANY_STRING);
        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("2");
        when(worstCase.getText()).thenReturn("baz");
        controller.validateInput();
    }

    @Test
    public void testEstimationComputationDefaultData1() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("1");
        when(nominalCase.getText()).thenReturn("2");
        when(worstCase.getText()).thenReturn("3");
        controller.computeEstimation(null);
        verify(estimation, times(1)).setText("2d 0h");
        verify(stdDeviation, times(1)).setText("0d 2h");
    }

    @Test
    public void testEstimationComputationDefaultData2() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("5");
        when(nominalCase.getText()).thenReturn("7");
        when(worstCase.getText()).thenReturn("13");
        controller.computeEstimation(null);
        verify(estimation, times(1)).setText("7d 5h");
        verify(stdDeviation, times(1)).setText("1d 2h");
    }

    @Test
    public void testEstimationComputationLowBorder() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("0");
        when(nominalCase.getText()).thenReturn("1");
        when(worstCase.getText()).thenReturn("2");
        controller.computeEstimation(null);
        verify(estimation, times(1)).setText("1d 0h");
        verify(stdDeviation, times(1)).setText("0d 2h");
    }

    @Test
    public void testEstimationComputationHighBorder() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("300");
        when(nominalCase.getText()).thenReturn("330");
        when(worstCase.getText()).thenReturn("365");
        controller.computeEstimation(null);
        verify(estimation, times(1)).setText("330d 6h");
        verify(stdDeviation, times(1)).setText("10d 6h");
    }

    @Test
    public void testEstimationComputationExtremeRange() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("0");
        when(nominalCase.getText()).thenReturn("1000");
        when(worstCase.getText()).thenReturn("2000");
        controller.computeEstimation(null);
        verify(estimation, times(1)).setText("1000d 0h");
        verify(stdDeviation, times(1)).setText("333d 2h");
    }

    @Test
    public void testEstimationComputationInvalidParams() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("0");
        when(nominalCase.getText()).thenReturn("0");
        when(worstCase.getText()).thenReturn("0");
        controller.computeEstimation(null);
        verify(estimation, times(1)).setText("N/A");
        verify(stdDeviation, times(1)).setText("N/A");
    }

    @Test
    public void itSetsErrorMessagesOnInvalidInput() {
        controller.createTask(null);
        verify(app, times(1)).setErrorMessage("Input Error: Task Name must not be empty.");
    }

    @Test
    public void itCreatesTaskObject() {
        when(taskName.getText()).thenReturn(ANY_STRING);

        when(bestCase.getText()).thenReturn("0");
        when(nominalCase.getText()).thenReturn("1");
        when(worstCase.getText()).thenReturn("2");
        when(estimation.getText()).thenReturn("123,45");
        when(stdDeviation.getText()).thenReturn("6,78");
        controller.createTask(null);
        verify(transaction, times(1)).start();
        verify(transaction, times(1)).store(any(ApplicationModel.class));
        verify(transaction, times(1)).end();
    }
}