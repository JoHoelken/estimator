package de.johoelken.estimator.controller;

import de.johoelken.estimator.Loader;
import de.johoelken.estimator.PathTo;
import de.johoelken.estimator.RoutingException;
import de.johoelken.estimator.helper.FXInitializer;
import de.johoelken.estimator.models.Task;
import de.johoelken.estimator.recording.Recorder;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class ApplicationControllerTest {

    @Mock
    private BorderPane body;
    @Mock
    private Button overviewBtn;
    @Mock
    private Loader loader;
    @Mock
    private Label activeTaskName;
    @Mock
    private Button activeTaskRecButton;
    @Mock
    private Button activeTaskStopButton;
    @Mock
    private FXMLLoader fxmlLoader;

    @Mock
    private Recorder recorder;

    private ApplicationController controller;

    @BeforeClass
    public static void setup() throws InterruptedException {
        FXInitializer.run();
    }

    @BeforeMethod
    public void fixture() throws RoutingException {
        MockitoAnnotations.initMocks(this);
        when(loader.getFXMLLoader(any(PathTo.class))).thenReturn(fxmlLoader);

        controller = new ApplicationController();
        controller.overviewBtn = overviewBtn;
        controller.body = body;
        controller.activeTaskRecButton = activeTaskRecButton;
        controller.activeTaskStopButton = activeTaskStopButton;
        controller.activeTaskName = activeTaskName;
        controller.recorder = recorder;
        controller.setLoader(loader);
    }

    @Test
    public void itDisablesOverviewButtonOnInitialization() {
        controller.initialize();
        verify(overviewBtn, times(1)).setDisable(true);
    }

    @Test
    public void itLoadsTheRequestedTarget() throws RoutingException  {
        when(fxmlLoader.getController()).thenReturn(mock(BaseController.class));

        controller.updateCenter(PathTo.NEW_TASK);

        verify(loader).getFXMLLoader(PathTo.NEW_TASK);
        verify(body, times(1)).setCenter(null);
        verify(overviewBtn, times(1)).setDisable(false);
    }

    @Test
    public void itSetsAnErrorMessageWhenControllerIsOfWrongType() throws RoutingException  {
        when(fxmlLoader.getController()).thenReturn(new Object());

        controller.updateCenter(PathTo.NEW_TASK);

        verify(loader).getFXMLLoader(PathTo.NEW_TASK);
        verify(body, never()).setCenter(null);
        verify(body, times(2)).setBottom(any(Node.class));
    }

    @Test
    public void itLoadsNewTaskFormWhenRequested() throws RoutingException {
        when(fxmlLoader.getController()).thenReturn(mock(BaseController.class));

        controller.createNewTask(null);

        verify(loader).getFXMLLoader(PathTo.NEW_TASK);
        verify(body, times(1)).setCenter(null);
        verify(overviewBtn, times(1)).setDisable(false);
    }

    @Test
    public void itLoadsOverviewWhenRequested() throws RoutingException  {
        when(fxmlLoader.getController()).thenReturn(mock(BaseController.class));

        controller.openOverview(null);

        verify(loader).getFXMLLoader(PathTo.OVERVIEW);
        verify(overviewBtn, times(1)).setDisable(true);
        verify(body, times(1)).setCenter(null);
    }

    @Test
    public void itSetsTheActiveTaskNameForDisplay() {
        Task task = new Task();
        task.setTaskName("Test");
        controller.setActiveTask(task);

        verify(recorder, times(1)).setActiveTask(task);
        verify(activeTaskName, times(1)).setText("Test");
        verify(activeTaskRecButton, times(1)).setVisible(true);
    }

    @Test
    public void itProvidesTheActiveTask() {
        Task task = new Task();
        task.setTaskName("Test");
        when(recorder.getActiveTask()).thenReturn(task);
        Assert.assertEquals(controller.getActiveTask(), task);
    }

    @Test
    public void itPassesRecordingCommandsToRecorder() {
        controller.startRecording(null);
        verify(recorder, times(1)).startRecording();

        controller.stopRecording(null);
        verify(recorder, times(1)).stopRecording();
    }

    @AfterMethod
    public void validate() {
        validateMockitoUsage();
    }
}
