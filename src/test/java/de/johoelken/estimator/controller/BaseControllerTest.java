package de.johoelken.estimator.controller;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BaseControllerTest {

    @Test
    public void testInjectionOfApplicationController() {
        ApplicationController app = new ApplicationController();
        BaseControllerImpl base = new BaseControllerImpl();
        base.setApplication(app);

        Assert.assertEquals(base.application(), app);
    }

    private class BaseControllerImpl extends BaseController {

    }

}