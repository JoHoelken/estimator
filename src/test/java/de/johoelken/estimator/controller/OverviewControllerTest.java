package de.johoelken.estimator.controller;

import de.johoelken.estimator.db.TestModel;
import de.johoelken.estimator.db.Transaction;
import de.johoelken.estimator.helper.FXInitializer;
import de.johoelken.estimator.models.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class OverviewControllerTest {

    @Mock
    private ApplicationController app;
    @Mock
    private Transaction transaction;

    @Mock
    private TableView taskTable;

    private OverviewController controller;

    @BeforeClass
    public static void setup() throws InterruptedException {
        FXInitializer.run();
    }

    @BeforeMethod
    public void fixture() {
        MockitoAnnotations.initMocks(this);

        controller = new OverviewController();
        controller.setApplication(app);
        controller.setTransaction(transaction);

        controller.setTaskTable(taskTable);
        setupTaskTable();
    }

    @Test
    public void itIsAnEstimatorController() {
        Assert.assertTrue(controller instanceof BaseController);
    }

    @Test
    public void itFetchesAllActiveTasksOnInitialization() {
        controller.initialize();
        verify(transaction, times(1)).findByQuery("SELECT t FROM Task t WHERE t.active = true");
    }

    @Test
    public void itWritesAllTasksToActiveTaskTable() {
        int numOfTasks = 5;
        when(transaction.findByQuery(anyString())).thenReturn(createTaskList(numOfTasks));

        ArgumentCaptor<ObservableList> captor = ArgumentCaptor.forClass(ObservableList.class);
        controller.loadTasks();

        verify(taskTable, times(1)).setItems(captor.capture());
        Assert.assertEquals(captor.getValue().size(), numOfTasks);
    }

    @Test
    public void itSetsAnErrorMessageIfTaskListResultIsNotAsExpected() {
        List<TestModel> invalidResult = new ArrayList<>();
        invalidResult.add(new TestModel());
        when(transaction.findByQuery(anyString())).thenReturn(invalidResult);

        controller.loadTasks();

        verify(app, times(1)).setErrorMessage(anyString());
        verify(taskTable, never()).setItems(any(ObservableList.class));
    }

    @Test
    public void itUpdatesTheSelectedTask() {
        Task task = new Task();
        TableView.TableViewSelectionModel model = mock(TableView.TableViewSelectionModel.class);
        when(taskTable.getSelectionModel()).thenReturn(model);
        when(model.getSelectedItem()).thenReturn(task);

        controller.updateSelectedTask(null);
        verify(app, times(1)).setActiveTask(task);
    }

    private List<Task> createTaskList(int numOfTasks) {
        List<Task> tasks = new ArrayList<>();
        for(int i=0; i<numOfTasks; i++) {
            Task t = new Task();
            t.setTaskName(String.format("TestTask_%s", i));
            t.setEstimation(i * 2.35);
            t.setStdDeviation(i * 0.9);
            tasks.add(t);
        }
        return tasks;

    }

    private void setupTaskTable() {
        String[] columns = { "Name", "Est.", "S1", "S2", "Actual","Options"};
        ObservableList<TableColumn> tableColumns = FXCollections.observableArrayList();
        for(String columName : columns) {
            TableColumn col = mock(TableColumn.class);
            when(col.getText()).thenReturn(columName);
            tableColumns.add(col);
        }
        when(taskTable.getColumns()).thenReturn(tableColumns);
    }
}