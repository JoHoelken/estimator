package de.johoelken.estimator.recording;

import de.johoelken.estimator.db.Transaction;
import de.johoelken.estimator.models.Task;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class RecorderTest {

    @Mock
    private Transaction transaction;

    private Recorder recorder;

    private Task task;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);

        recorder = new Recorder(transaction);
        task = new Task();
    }

    @Test
    public void testSetActiveTask() {
        recorder.setActiveTask(task);
        Assert.assertEquals(recorder.getActiveTask(), task);
    }

    @Test
    public void testStartRecordingWithoutActiveTask() {
        recorder.startRecording();
        Assert.assertNull(recorder.getRunningTask());
    }

    @Test
    public void testStartRecording() {
        recorder.setActiveTask(task);
        recorder.startRecording();
        Assert.assertEquals(recorder.getRunningTask(), task);
    }

    @Test
    public void testTimeRecording() throws InterruptedException{
        recorder.setActiveTask(task);
        recorder.unitFactor = 10;
        recorder.startRecording();
        Thread.sleep(30);
        Assert.assertEquals(recorder.getRecordedTime(), 3L);
    }

    @Test
    public void testRecordedTimeAggregation() throws InterruptedException{
        task.setActualTime(10L);
        recorder.setActiveTask(task);
        recorder.unitFactor = 10;

        recorder.startRecording();
        Thread.sleep(20);
        recorder.stopRecording();

        Assert.assertEquals(task.getActualTime(), 12L);
        verify(transaction, times(1)).update(task);
        Assert.assertNull(recorder.getRunningTask());
    }

    @Test
    public void testRunningTaskCheck() {
        Assert.assertFalse(recorder.isRunningTask(task));

        recorder.setActiveTask(task);
        recorder.startRecording();

        Assert.assertTrue(recorder.isRunningTask(task));
        Assert.assertFalse(recorder.isRunningTask(null));

        Task other = mock(Task.class);
        when(other.getId()).thenReturn(123L);
        Assert.assertFalse(recorder.isRunningTask(other));
    }

}