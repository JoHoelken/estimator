package de.johoelken.estimator;

import de.johoelken.estimator.helper.FXInitializer;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class MainTest {

    @Mock
    Stage primaryStage;

    @BeforeClass
    public static void setup() throws InterruptedException {
        FXInitializer.run();
    }

    @BeforeMethod
    public void fixture() throws RoutingException {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testTheStartSequence() {
        Main app = new Main();
        app.start(primaryStage);

        verify(primaryStage, times(1)).setTitle("Estimator");
        verify(primaryStage, times(1)).setScene(any(Scene.class));
        verify(primaryStage, times(1)).show();
    }
}