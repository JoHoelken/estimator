package de.johoelken.estimator.db;

import de.johoelken.estimator.models.ApplicationModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TestModel extends ApplicationModel {

    @Id
    @Column(name = "TEST_IT", nullable = false)
    @GeneratedValue
    private long id;

    @Column(name = "Test_NAME", nullable = false)
    private String name;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
