package de.johoelken.estimator.db;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class DefaultTransactionTest {

    @Test
    public void itCanInitialize() {
        Transaction transaction = new DefaultTransaction();
        Assert.assertNotNull(transaction);
    }

    @Test
    public void itCanStart() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();
    }

    @Test
    public void itCanStop() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();
        transaction.end();
    }

    @Test
    public void itCanStore() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();
        TestModel model = new TestModel();
        model.setName("UnitTestModel");
        transaction.store(model);
        transaction.commit();

        TestModel result = transaction.findById(TestModel.class, 1L);
        transaction.end();

        clearDB();

        Assert.assertNotNull(result);
        Assert.assertEquals(model.getName(), result.getName());
    }

    @Test
    public void itCanUpdate() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();
        TestModel model = new TestModel();
        model.setName("UnitTestModel");
        transaction.store(model);
        transaction.commit();

        TestModel intermediateResult = transaction.findById(TestModel.class, 2L);
        if(intermediateResult == null || !"UnitTestModel".equals(intermediateResult.getName())) {
            transaction.end();
            clearDB();
            Assert.fail("Saving of the original model failed.");
        }

        model.setName("FooBarBaz");
        transaction.update(model);
        transaction.commit();

        TestModel result = transaction.findById(TestModel.class, 2L);
        transaction.end();

        clearDB();
        Assert.assertEquals(model.getName(), result.getName());
    }

    @Test
    public void itCanDelete() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();
        TestModel model = new TestModel();
        model.setName("UnitTestModel");
        transaction.store(model);
        transaction.commit();

        transaction.delete(model);
        transaction.commit();

        TestModel result = transaction.findById(TestModel.class, 1L);
        transaction.end();

        Assert.assertNull(result);
    }

    @Test
    public void itCanFindByJPQL() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();

        int numOfModels = 5;
        for(int idx=0; idx<numOfModels; idx++) {
            TestModel model = new TestModel();
            model.setName(String.format("UnitTestModel_%s", idx));
            transaction.store(model);
        }
        transaction.commit();

        List<TestModel> entries = transaction.findByQuery("SELECT t FROM TestModel t");

        transaction.end();
        clearDB();

        Assert.assertEquals(numOfModels, entries.size());
    }



    private void clearDB() {
        Transaction transaction = new DefaultTransaction("test");
        transaction.start();
        List<TestModel> entries = transaction.findByQuery("SELECT t FROM TestModel t");
        for(TestModel model : entries) {
            transaction.delete(model);
        }
        transaction.end();
    }


}