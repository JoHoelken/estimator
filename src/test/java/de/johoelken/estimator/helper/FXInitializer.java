package de.johoelken.estimator.helper;

import javafx.application.Application;
import javafx.stage.Stage;

public final class FXInitializer  {

    private static final Object barrier = new Object();

    private static FXInitializer instance;

    private boolean initiated;

    private FXInitializer() {
        //Nothing to do
    }

    private static FXInitializer getInstance() {
        if(instance == null) {
            instance = new FXInitializer();
        }
        return instance;
    }

    public static void run() throws InterruptedException {
        getInstance().initialize();
    }

    private void initialize() throws InterruptedException {
        if(initiated) {
            return;
        }

        System.setProperty("testfx.robot", "glass");
        System.setProperty("testfx.headless", "true");
        System.setProperty("prism.order", "sw");
        System.setProperty("prism.text", "t2k");

        Thread t = new Thread("JavaFX Init Thread") {
            @Override
            public void run() {
                Application.launch(FXApp.class);
            }
        };

        t.setDaemon(true);
        t.start();
        initiated = true;

        synchronized(barrier) {
            barrier.wait();
        }
    }


    public static class FXApp extends Application {
        @Override
        public void start(Stage primaryStage) throws Exception {
            synchronized(barrier) {
                barrier.notifyAll();
            }
        }
    }

}
