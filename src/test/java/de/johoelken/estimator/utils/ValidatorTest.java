package de.johoelken.estimator.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ValidatorTest {

    @Test
    public void itShouldDetectNullValues() {
        Assert.assertTrue(Validator.isEmptyOrNull(null));
    }

    @Test
    public void itShouldDetectBlankStrings() {
        Assert.assertTrue(Validator.isEmptyOrNull(""), "Empty String not detected as blank");
        Assert.assertTrue(Validator.isEmptyOrNull(" "), "Space character not detected as blank");
        Assert.assertTrue(Validator.isEmptyOrNull("\t"), "Tab character not detected as blank");
        Assert.assertTrue(Validator.isEmptyOrNull("\n"), "Line Break character not detected as blank");
    }

    @Test
    public void itShouldDetectRealStrings() {
        Assert.assertFalse(Validator.isEmptyOrNull("any non empty string."));
    }
}