package de.johoelken.estimator.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FormatterTest {

    @Test
    public void itFormatsSecondsToWorkingDays() {
        long longTimeNow = 78 * 28800 + 7 * 3600 + 58 * 60 + 34;
        Assert.assertEquals(Formatter.toWorkingDayFormat(longTimeNow, true), "78d 7h 58m");
        Assert.assertEquals(Formatter.toWorkingDayFormat(longTimeNow, false), "78d 7h");
    }

    @Test
    public void itFormatsZeroSecondsToWorkingDays() {
        Assert.assertEquals(Formatter.toWorkingDayFormat(0L, true), "0d 0h 0m");
        Assert.assertEquals(Formatter.toWorkingDayFormat(0L, false), "0d 0h");
    }

    @Test
    public void itIgnoresSecondsToTheMinuteWhenFormattingToWorkingDay() {
        Assert.assertEquals(Formatter.toWorkingDayFormat(55L, true), "0d 0h 0m");
    }

    @Test
    public void itIgnoresSecondsToTheHourWhenFormattingToWorkingDayIgnoreMinutes() {
        Assert.assertEquals(Formatter.toWorkingDayFormat(58 * 60, false), "0d 0h");
    }

    @Test
    public void itFormatsALotOfSecondsToWorkingDays() {
        long longTimeNow = 356 * 28800 + 7 * 3600 + 58 * 60 + 59;
        Assert.assertEquals(Formatter.toWorkingDayFormat(longTimeNow, true), "356d 7h 58m");
        Assert.assertEquals(Formatter.toWorkingDayFormat(longTimeNow, false), "356d 7h");
    }
}