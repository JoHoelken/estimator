Welcome to the ESTIMATOR. 

The ESTIMATOR is a free light-weight GUI application to perform task estimation based on 
[PERT](https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique).

## Status

The project is in initial pre-release state. Some [screenshot](https://gitlab.com/JoHoelken/estimator/wikis/home) impressions can be found in the [wiki](https://gitlab.com/JoHoelken/estimator/wikis/home)

## Install / Run
Currently no downloadable package/release is available. You have to checkout the repository and build it with [maven](https://maven.apache.org/). To create a native launcher use the following line

```bash
mvn clean jfx:native
```

This will create a runnable application in `target/jfx/native/estimator-{version}` folder.

## Contributing
All contributions are very welcome! Please read the [contribution guide](CONTRIBUTING.md) for legal details and code of conduct. 

## LICENCE
GPLv3, see [LICENCE](LICENCE.md) for details.
